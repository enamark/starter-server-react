import './App.css';
import {useState} from 'react';
function App() {
  const [colors,setColors]=useState();
  const [user,setUser] = useState();
  const [error,setError]=useState();
  const [userProxy,setUserProxy]=useState();
  const getColors=async()=>{
    const response=await fetch('http://localhost:5000/colors')
    const data=await response.json();
    console.log({data});
    setColors(data)
  }

  const getUserFromCORSDisabledServer=async()=>{
    const response=await fetch('http://localhost:6000/user')
    const data=await response.json();
    console.log({data});
    setColors(data)
  }

  const getUser = async () => {
    const response = await fetch('http://localhost:5000/user');
    const data = await response.json();
    console.log({data});
    setUser(data);
  }

  const getUserProxy = async () => {
    const response = await fetch('http://localhost:5000/api/user');
    const data = await response.json();
    console.log({data});
    setUserProxy(data);
  }

  const getCookie = async () => {
    const response = await fetch('http://localhost:5000/set-cookie', {
      method: 'GET',
      credentials: 'include'
    });
    const data = await response.json();
    console.log(data);
    console.log(document.cookie);
  }

  return (
      <div className="App">
        <header className="App-header">
          <p>
            Welcome to the <b>colors app! 🌈</b>
          </p>
          <button
              onClick={getColors}
          >
            GET server/colors
          </button>
          <p>
            {
                colors && JSON.stringify(colors)
            }
          </p>
        </header>

        <div>
          <button onClick={getUserFromCORSDisabledServer}>
            GET cors-disabled-server/user (will fail)
          </button>
          <p>
            {
                error && JSON.stringify(error)
            }
          </p>
        </div>

        <div>
          <button onClick={getUser}>
            GET server/user
          </button>
          <p>
            {
                user && JSON.stringify(user)
            }
          </p>
        </div>

        <div>
          <button onClick={getUserProxy}>
            GET server/api/user
          </button>
          <p>
            {
                userProxy && JSON.stringify(userProxy)
            }
          </p>
        </div>

        <div>
          <button onClick={getCookie}>
            GET server/set-cookie
          </button>
        </div>
      </div>
  );
}

export default App;