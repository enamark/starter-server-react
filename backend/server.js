const express = require('express');
const app = express();
const port = 5000;
const { createProxyMiddleware } = require('http-proxy-middleware');
const cors = require('cors');

const options = {
    origin: 'http://localhost:3000',
    credentials: true,
}
app.use(cors(options))

// app.use(require('cookie-parser')());

app.get("/set-cookie",(req,res)=> {
    const cookieValue = `cookiess ${Math.random()}`
    res.cookie("cookie", cookieValue).send({ data: { name: "cookie", value: cookieValue }})
})

// this means everything prepended with /api will be proxied to the target
app.use('/api', createProxyMiddleware({
    target: 'http://cors-disabled-backend:6000',
    changeOrigin: true
}));

// I thought this would work... it doesnt actually FAIL but it doesnt get any data back.
// im unsure why.
app.get('/user',async(req,res)=>{
    try {
        const response= await axios.get('http://localhost:6000/user');
        res.json(response)
    } catch (e) {
        res.json(e)
    }
})

// this is from our own server so no proxy needed. we can access it bc of the cors thing on line 16
app.get('/colors',(req,res)=>{
    const colors = {"colors":[
            {
                "color": "red",
                "value": "#f00"
            },
            {
                "color": "green",
                "value": "#0f0"
            },
            {
                "color": "blue",
                "value": "#00f"
            },
            {
                "color": "cyan",
                "value": "#0ff"
            },
            {
                "color": "magenta",
                "value": "#f0f"
            },
            {
                "color": "yellow",
                "value": "#ff0"
            },
            {
                "color": "black",
                "value": "#000"
            }
        ]
    }
    res.json(colors)
})

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
});