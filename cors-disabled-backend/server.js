const express=require('express');
const port = 6000;
const app=express();

app.get('/user',(req,res)=>{
    console.log("calling user in disabled backend")
    res.send({
        'name':'FuzzySid',
        'age':'22'
    })
})

app.listen(port,()=>console.log('CORS Disabled Server is Up and Running...'))