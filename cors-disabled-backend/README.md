This backend is in here to demonstrate how you actual backend 
can act as a proxy server to access other backends that dont have cors enabled. 

So frontend trying to hit this backend will be blocked by cors
but if we have out frontend hit our backend and then have it hit
this one, we can scoot around the cors issue.